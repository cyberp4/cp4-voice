.ONESHELL:

PROJECT_NAME=cp4_voice

PKG_NAME=$(PROJECT_NAME)

HOST=rpi-p4
REMOTE_PATH=

default: build

.PHONY: lint
lint:
	mypy $(PROJECT_NAME)

.PHONY: build
build: assets
	@mkdir -p dist/arch
	if test -f dist/*.whl ; then mv dist/*.whl dist/arch ; fi
	python3 setup.py bdist_wheel

.PHONY: deploy
deploy:
	ssh $(HOST) 'rm $(PROJECT_NAME)-*-py3-none-any.whl'
	scp dist/*.whl $(HOST):$(REMOTE_PATH)
	ssh $(HOST) 'pip install $(PKG_NAME)-*-py3-none-any.whl --force-reinstall --no-deps --no-warn-script-location'

.PHONY: samples
samples:
	@(cd samples && rm -f *.mp3 && ./make_samples.py)

.PHONY: assets
assets:
	@mkdir -p $(PKG_NAME)/samples
	rm -f $(PKG_NAME)/samples/*.mp3
	cp samples/*.mp3 $(PKG_NAME)/samples/

.PHONY: samples-clean
samples-clean:
	@(cd samples && rm -f *.mp3)

.PHONY: update-config
update-config:
	scp systemd/cp4-voice.service $(HOST):$(REMOTE_PATH)

.PHONY: restart
restart:
	ssh $(HOST) 'sudo systemctl restart cp4-voice'
