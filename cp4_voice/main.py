import json
import logging
import os
import random
import subprocess
from pathlib import Path
from random import choice
from threading import Timer
import itertools
import typing
import argparse

import pika
from pika.exceptions import AMQPError
from pika.channel import Channel

from .log import LogMgr
from . import VERSION

__author__ = 'Eric Pascual'

SAMPLES_DIR = Path(__file__).parent / "samples"

random.seed()

logger: logging.Logger


class Context:
    delayed_message: typing.Union[Timer, None] = None
    reminder_messages: list[Path] = []

    @classmethod
    def schedule_vocal_reminder(cls,
                                base_delay: float = 5.0,
                                first_reminder=True,
                                exclude_sample: Path = None,
                                samples: list[Path] = None
                                ):
        delay = base_delay * (1 + random.random())
        logger.info("scheduling reminder in %fs (first: %s)", delay, first_reminder)

        cls.cancel_vocal_reminder()
        if first_reminder:
            if samples:
                cls.reminder_messages = samples
            else:
                cls.reminder_messages = list(SAMPLES_DIR.glob('hurry_up_*.mp3')) + list(
                    SAMPLES_DIR.glob('your_turn_*.mp3'))
                if exclude_sample:
                    cls.reminder_messages = [p for p in cls.reminder_messages if p != exclude_sample]

            random.shuffle(cls.reminder_messages)

        cls.delayed_message = Timer(delay, awake_player)
        cls.delayed_message.start()

    @classmethod
    def cancel_vocal_reminder(cls):
        if cls.delayed_message:
            cls.delayed_message.cancel()
            cls.delayed_message = None


def awake_player():
    play_sample(Context.reminder_messages.pop())
    if Context.reminder_messages:
        Context.schedule_vocal_reminder(first_reminder=False)
    else:
        logger.info("no more reminder to say => staying silent from now")


def play_sample(name_or_names: typing.Union[Path, str, typing.List]) -> typing.Union[Path, None]:
    if isinstance(name_or_names, Path):
        path = name_or_names
    else:
        if isinstance(name_or_names, str):
            names = [name_or_names]
        else:
            names = name_or_names

        patterns = [f"{name}*.mp3" for name in names]
        choices = list(itertools.chain(*(SAMPLES_DIR.glob(pattern) for pattern in patterns)))

        try:
            path = choice(choices)
        except IndexError:
            logger.error('sample not found: %s', ', '.join(names))
            return None

    logger.info('playing sample "%s"...', path.name)
    subprocess.run(f"mpg123 -q {path} > /dev/null", shell=True)
    return path


_winner_sample = {
    'human': 'you_won',
    'machine': 'i_won',
    None: 'null_game'
}


def handle_game_over(time: float, payload: dict, channel: Channel):
    Context.cancel_vocal_reminder()
    if not payload:
        return

    winner: str = payload['winner']
    if winner:
        winner = winner.lower()

    sample = _winner_sample.get(winner)
    if sample:
        logger.info("saying : game over (winner=%s)", winner)
        play_sample(sample)

        reminders = [first_reminder := SAMPLES_DIR / Path('play_again_1.mp3')]
        if winner == "machine" or winner is None:
            turns_played = payload.get('turns_played', 0)
            if turns_played > 15:
                play_sample('well_done')
            elif 0 < turns_played <= 5:
                play_sample('too_easy')

            reminders.extend([p for p in SAMPLES_DIR.glob('play_again_*.mp3') if p != first_reminder])

        Context.schedule_vocal_reminder(samples=reminders)


def _announce_player(player: str):
    human_turn = player.lower() == 'human'

    sample = play_sample("your_turn" if human_turn else "my_turn")

    # implement eagerness trait if human's turn to play
    if human_turn:
        Context.schedule_vocal_reminder(exclude_sample=sample)


def handle_game_started(time: float, payload: dict, channel: Channel):
    Context.cancel_vocal_reminder()

    play_sample("game_started")

    _announce_player(payload['player'])


def handle_player_change(time: float, payload: dict, channel: Channel):
    Context.cancel_vocal_reminder()

    _announce_player(payload['player'])


def handle_token_played(time: float, payload: dict, channel: Channel):
    Context.cancel_vocal_reminder()


def handle_state_change(time: float, payload: dict, channel: Channel):
    new_state = payload['state'].lower()
    logger.info(f"saying : {new_state}")
    play_sample(new_state)


def handle_token_detected(time: float, payload: dict, channel: Channel):
    column_number = payload['column']
    sample = f"token_detected_{column_number}"
    play_sample(sample)


def _basic_handler(sample_name: str) -> typing.Callable:
    def _handler(*args):
        Context.cancel_vocal_reminder()
        play_sample(sample_name)

    return _handler


handlers = {
    "game_over": handle_game_over,
    "game_started": handle_game_started,
    "player_changed": handle_player_change,
    "token_played": handle_token_played,

    "system_almost_ready": _basic_handler('system_almost_ready'),
    "system_ready": _basic_handler('system_ready'),
    "shutdown_started": _basic_handler('shutdown_started'),
    "shutdown_requested": _basic_handler('prepare_for_power_off'),
    "powering_off": _basic_handler('powering_off'),
    "ready_for_refilling": _basic_handler('ready_for_refilling'),
    "token_detected": handle_token_detected,
}


def main():
    """ Produces vocal messages based on notification messages circulating on the bus
    """
    parser = argparse.ArgumentParser(description=f"CyberP4 Voice (version {VERSION})")
    parser.add_argument('--username', '-u')
    parser.add_argument('--password', '-p')
    parser.add_argument('--verbose', '-v', action='store_true')

    args = parser.parse_args()

    global logger
    logger = LogMgr.setup(args.verbose)

    logger.info(f"starting {parser.description}...")

    # check if mpg123 is installed
    if subprocess.call("which mpg123", shell=True) != 0:
        raise SystemExit('cannot start: mpg123 is not installed on the system')

    username = args.username or os.getenv('CP4_RMQ_USERNAME')
    password = args.password or os.getenv('CP4_RMQ_PASSWORD')

    if not (username and password):
        raise SystemExit('cannot start: RMQ username or password not provided')

    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host='localhost',
                credentials=pika.PlainCredentials(
                    username=username or 'guest',
                    password=password or 'guest'
                )
            )
        )

    except AMQPError as e:
        raise SystemExit(f"cannot start: message bus connection failed: {e!r}")

    channel = connection.channel()

    channel.exchange_declare(exchange='cp4', exchange_type='topic')

    result = channel.queue_declare('', exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange='cp4', queue=queue_name, routing_key='*')

    def message_received(channel, method_frame, header_frame, body: bytes):
        routing_key = method_frame.routing_key
        body = json.loads(body.decode())
        logger.info("%s : %s", routing_key, body)

        # to avoid several announces to be spoken at the same time
        Context.cancel_vocal_reminder()

        handler = handlers.get(routing_key)
        if handler:
            timestamp = body.pop('__time__')
            handler(timestamp, body, channel)

        else:
            logger.info("... nothing to say")

    channel.basic_consume(queue=queue_name, on_message_callback=message_received, auto_ack=True)

    try:
        logger.info("Listening to messages...")
        channel.start_consuming()

    except KeyboardInterrupt:
        logger.info('!! interrupted !!')

    except AMQPError as e:
        logger.error("Unexpected AMQP error while listening: %s", e)

    finally:
        channel.stop_consuming()
        connection.close()


def test_sample(name: str):
    global logger
    logger = LogMgr.setup(verbose=True)
    play_sample(name)


if __name__ == '__main__':
    main()
