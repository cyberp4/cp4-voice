# CyberP4 voice synthesis component

## Features

Pronounce vocal messages corresponding to game events.

Events are listened on the message bus (RabbitMQ) used for communication between the system
components.

They are associated to audio files contained the vocalized messages, pre-generated thanks
to the Google Translate API.

## Development

Have a look at the Makefile targets for the project build and deployment tasks

## Dependencies

- Python 3.9.2
- packages : see `requirements.txt` file
- mpg123 (`apt-get install mpg123`)
