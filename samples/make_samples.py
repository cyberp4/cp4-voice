#! /usr/bin/env python

from pathlib import Path
from subprocess import run

try:
    from gtts import gTTS


    def generate_sample(sample_name: str, text: str, output_path: Path):
        print(f"generating {sample_name}.mp3...")
        tts = gTTS(text, lang='fr')
        tts.save(str(output_path))

except ImportError:
    print("gTTS not installed. Fallback to API direct calls")

    from urllib.request import urlopen
    from urllib.parse import quote_plus


    def generate_sample(sample_name: str, text: str, output_path: Path):
        q_parm = quote_plus(text.strip())
        url = f"http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q={q_parm}&tl=fr"
        print(f"generating {sample_name}.mp3...")
        with urlopen(url) as stream_fp:
            with output_path.open('wb') as sample_fp:
                sample_fp.write(stream_fp.read())


def change_sample_speed(input_path: Path, output_path: Path, ratio: float):
    command = f'ffmpeg -v 0 -i {input_path} -filter:a "atempo={ratio}" -y {output_path}'
    # print(command)
    run(command, shell=True)


with Path("samples.msg").open() as fp:
    TEMP_MP3 = Path("/tmp/sample.mp3")
    SPEED_RATIO = 1.2

    for message in fp.readlines():
        sample_name, message = message.split(":", maxsplit=1)
        generate_sample(sample_name, message, TEMP_MP3)
        change_sample_speed(TEMP_MP3, Path(f"{sample_name}.mp3"), SPEED_RATIO)

    Path(TEMP_MP3).unlink()
