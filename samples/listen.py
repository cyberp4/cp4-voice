#! /usr/bin/env python

import sys
from subprocess import call

file_path = sys.argv[1]
call(f"ffplay -nodisp -autoexit -loglevel quiet {file_path}", shell=True)
