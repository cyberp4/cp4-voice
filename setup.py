from setuptools import setup
from pathlib import Path

setup(
    name='cp4_voice',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=['cp4_voice'],
    package_data={
        '': ['samples/*.mp3']
    },
    url='',
    license='',
    author='Eric Pascual',
    author_email='',
    description='',
    python_requires='>=3.9.2',
    install_requires=Path('requirements.txt').read_text(),
    entry_points={
        'console_scripts': [
            'cp4-voice=cp4_voice.main:main'
        ]
    }
)
